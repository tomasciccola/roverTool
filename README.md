# roverTool

small cli tool to display images of the mars rover as ascii art on the terminal

it also has an API which exposes three methods through a node module:

```
getLastImage --> get the last generated image 

getImageAt(imageId) --> get specific image with id

imagesIterator(interval) --> get an async iterator which will yield every image incrementaly, given an interval in seconds

```

every method of the API returns a json object in the form of
```
{
  metadata:{
    id, 
    sol, 
    camera,
    img_src,
    earth_date,
    rover: {
      id,
      name,
      landing_date,
      launch_date,
      status
    }
  },
  images:{
    ascii,
    base64,
    index
  }
}

```

to use the tool run

```
npm install
```

then follow instructions running

```
./cli
```
