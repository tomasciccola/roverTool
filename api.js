const fetch = require('node-fetch')

const ENDPOINT_ROOT = 'https://hiring.hypercore-protocol.org/termrover' 


var timer = function(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
};

async function getImg(path){
  const endpoint = new URL(`${ENDPOINT_ROOT}/${path}`)
  try{
    const res = await fetch(endpoint)
    const json = await res.json()
    return json
  }catch(e){
    console.error(`error getting image from path: ${endpoint}`)
  }
}

module.exports.getLastImage = async function(){
  return await getImg('latest')
}

module.exports.getImageAt = async function(at){
  return await getImg(at)
}

module.exports.imagesIterator = async function*(interval){
  let at = 0
  let nImgs = 0 
  try{
    const res = await fetch(ENDPOINT_ROOT)
    const json = await res.json()
    nImgs = json.numImages
  }catch(e){
    console.error('error getting the number of images')
  }
  while (at  < nImgs){
    const json = await getImg(at)
    at += 1
    await timer(interval)
    yield json
  }
}
